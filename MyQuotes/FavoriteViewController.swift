//
//  FavoriteViewController.swift
//  MyQuotes
//
//  Created by Indoalliz on 17/03/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class FavoriteViewController: UIViewController {

    @IBOutlet weak var FavoriteImg: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let savingDefaults = UserDefaults.standard
        let indexSaved = savingDefaults.integer(forKey: "favorite")
        
        let imageName = "quotes-\(indexSaved)"
        let image = UIImage.init(named: imageName)
        
        FavoriteImg.image = image
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}
