//
//  SettingViewController.swift
//  MyQuotes
//
//  Created by Indoalliz on 17/03/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    @IBOutlet var settingLabels: [UILabel]!
    @IBOutlet weak var segmentedTheme: UISegmentedControl!
    @IBOutlet weak var stepperFavorite: UIStepper!
    @IBOutlet weak var switchBorder: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func segmentedChange(_ sender: Any) {
    }
    @IBAction func switchChanged(_ sender: Any) {
    }
}
