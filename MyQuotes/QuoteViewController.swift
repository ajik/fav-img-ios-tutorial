//
//  QuoteViewController.swift
//  MyQuotes
//
//  Created by Indoalliz on 17/03/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

var theme: UIColor = UIColor.black

class QuoteViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var NextBtn: UIButton!
    @IBOutlet weak var quoteImgView: UIImageView!
    var quoteArray = [String]()
    var arrayIndex = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backBtn.isEnabled = false
        
        for i in 1..<11 {
            let quotesPrefix: String = "quotes-"
            let quotesSuffix: String = String(i)
            let quotesName = quotesPrefix + quotesSuffix
            quoteArray.append(String(quotesName))
        }

        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnClick(_ sender: Any) {
        
        if arrayIndex > 0 {
            
            arrayIndex -= 1
            if arrayIndex == 0 {
                backBtn.isEnabled = false
            }
            let index = quoteArray[arrayIndex]
            let quoteImage = UIImage(named: index)
            quoteImgView.image = quoteImage
            NextBtn.isEnabled = true
            print(arrayIndex)
            
        }
    }
    
    @IBAction func NextBtnClick(_ sender: Any) {
        
        if arrayIndex <= quoteArray.count - 1 {
            NextBtn.isEnabled = true
            backBtn.isEnabled = true
            
            let index = quoteArray[arrayIndex]
            // print(index)
            let quoteImage = UIImage(named: index)
            arrayIndex += 1
            quoteImgView.image = quoteImage
            
            // print(arrayIndex)
            // print(quoteArray.count - 1)
            if arrayIndex == quoteArray.count {
                // print(arrayIndex)
                // print(quoteArray.count)
                NextBtn.isEnabled = false
            }
        }
    }
    
    @IBAction func favoriteBtnClick(_ sender: Any) {
        
        let savingDefault = UserDefaults.standard
        savingDefault.set(arrayIndex, forKey: "favorite")
    }
}
